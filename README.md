#Python code challange
####Implement a script in python that as input takes an URL and as output displays statistics of words found in the content from the input URL.

##Requirements
App developed and tested using Python 3.7

Requirements for the app you will find in requirements.txt file

You can install these requirements by typing following command:

``pip install -r requirements.txt``

##Starting app

You can check how to start app and what arguments it accepts by typing following command:

``python word_stats.py -h ``

The output would be:


```
usage: word_stats.py [-h] [-m MAX_ITEMS] [-c {sensitive,ignore}]
                       [-p {graph,console}]
                       url
  
  positional arguments:
    url                   website url
  
  optional arguments:
    -h, --help            show this help message and exit
    -m MAX_ITEMS, --max_items MAX_ITEMS
                          Max items to be displayed
    -c {sensitive,ignore}, --case_sensitive {sensitive,ignore}
                          Case sensitive calculation
    -p {graph,console}, --presentation {graph,console}
                          How result should be presented
```

##Sample usage
``python word_stats.py https://onet.pl --presentation console --max_items 20 --case_sensitive ignore
``

###Sample output
#### console
``[('w', 68), ('na', 61), ('i', 47), ('nie', 35), ('z', 27), ('się', 27), ('o', 23), ('do', 21), ('to', 15), ('jak', 14), ('warszawa', 13), ('dla', 13), ('więcej', 12), ('po', 11), ('za', 11), ('jest', 11), ('onet', 9), ('tv', 9), ('metropolia', 9), ('gzm', 9)]
``
#### or graph
![Image](https://i.ibb.co/vVJmFWw/Screenshot-2019-05-13-at-07-31-23.png
)

##Tests

The app includes few simple unit tests.
 
You can run them from IDE like IntelliJ Idea or from command line using following command:
``python -m unittest test.retriever.test_website_retriever.TestWebsiteStats``

##Summary

Thank you :)