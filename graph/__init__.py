from collections import Counter

import numpy as np
import matplotlib.pyplot as plt

from logger import logger


def draw_graph(stats):
    logger.debug("Creating graph..")
    labels, values = zip(*stats)

    indexes = np.arange(len(labels))
    width = 1

    plt.bar(indexes, values, width)
    plt.xticks(indexes + width * 0.5, labels, rotation='vertical')
    plt.show()
