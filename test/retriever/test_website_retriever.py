import unittest
from unittest import mock

from retriever import website_retriever
from utils import utils


class TestWebsiteStats(unittest.TestCase):

    test_html = ""
    output_words = ""

    @staticmethod
    def setUpClass():
        TestWebsiteStats._read_test_data()

    @classmethod
    def _read_test_data(cls):
        path_html = utils.get_project_root().joinpath('_resources/test.html')
        path_txt = utils.get_project_root().joinpath('_resources/test.txt')

        with path_html.open() as f:
            cls.test_html = f.read()

        with path_txt.open() as f:
            cls.output_words = f.read()


    @staticmethod
    def _mock_response(
            status=200,
            content="CONTENT",
            raise_for_status=None):

        mock_resp = mock.Mock()
        # mock raise_for_status call w/optional error
        mock_resp.raise_for_status = mock.Mock()
        if raise_for_status:
            mock_resp.raise_for_status.side_effect = raise_for_status
        # set status code and content
        mock_resp.status_code = status
        mock_resp.content = content

        return mock_resp

    @mock.patch('requests.get')
    def test_retrieve_website_content(self, mock_get):

        mock_resp = self._mock_response(content=self.test_html)
        mock_get.return_value = mock_resp

        result = website_retriever.retrieve_website_content("http://onet.pl")
        self.assertEqual(self.output_words, result)


if __name__ == '__main__':
    unittest.main()
