import unittest

from stats import stats_calculator


class TestWebsiteStats(unittest.TestCase):
    WORDS = "Raz,raz,dwa,raz,trzy"

    def test_case_insensitive_count(self):
        counter = stats_calculator.calculate_statistics(self.WORDS)

        self.assertEqual(counter.get("raz"), 3)
        self.assertEqual(counter.get("dwa"), 1)
        self.assertEqual(counter.get("trzy"), 1)

    def test_case_sensitive_count(self):
        counter = stats_calculator.calculate_statistics(self.WORDS, case_sensitive=True)

        self.assertEqual(counter.get("Raz"), 1)
        self.assertEqual(counter.get("raz"), 2)
        self.assertEqual(counter.get("dwa"), 1)
        self.assertEqual(counter.get("trzy"), 1)


if __name__ == '__main__':
    unittest.main()
