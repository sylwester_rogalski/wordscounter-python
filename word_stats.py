import argparse
import getopt
import sys
from enum import Enum
from urllib.parse import urlparse

from graph import draw_graph
from logger import logger
from retriever import website_retriever
from stats import stats_calculator


class WebsiteStats:
    class Presentation(Enum):
        GRAPH = "graph"
        CONSOLE = "console"

    class CaseSensitive(Enum):
        SENSITIVE = "sensitive"
        IGNORE = "ignore"

    max_items = None
    presentation = Presentation.CONSOLE
    case_sensitive = CaseSensitive.IGNORE
    url = ''

    def main(self):
        self.parse_params()
        self.validate_url()

        content = website_retriever.retrieve_website_content(self.url)

        is_case_sensitive = True if self.case_sensitive == self.CaseSensitive.SENSITIVE.value else False
        stats = stats_calculator.calculate_statistics(content, is_case_sensitive)
        self.show_results(stats)

    def show_results(self, stats):
        logger.debug("Presenting results")
        if self.max_items is None:
            self.max_items = len(stats)

        if self.presentation == WebsiteStats.Presentation.CONSOLE.value:
            print(stats.most_common(self.max_items))
        elif self.presentation == WebsiteStats.Presentation.GRAPH.value:
            draw_graph(stats.most_common(self.max_items))

    def parse_params(self):
        logger.debug("Parsing parameters")
        # skip name of the script

        parser = argparse.ArgumentParser()
        parser.add_argument("url", help="website url")
        parser.add_argument('-m', '--max_items', type=int, help="Max items to be displayed")
        parser.add_argument('-c', '--case_sensitive', choices=[self.CaseSensitive.SENSITIVE.value, self.CaseSensitive.IGNORE.value], help="Case sensitive calculation")
        parser.add_argument('-p', '--presentation', choices=[self.Presentation.GRAPH.value, self.Presentation.CONSOLE.value], help="How result should be presented")
        args = parser.parse_args()

        self.url = args.url
        self.presentation = args.presentation
        self.max_items = args.max_items
        self.case_sensitive = args.case_sensitive

    def validate_url(self):
        logger.debug("URL validation")

        url_elements = urlparse(self.url)

        if not url_elements.scheme:
            print("Given URL doesn't contain scheme: " + self.url)
            print("Type full URL with scheme eg. https://google.com")

            sys.exit()


if __name__ == '__main__':
    # read commandline arguments
    websiteStats = WebsiteStats()
    websiteStats.main()
