import re
import string
import sys

import requests
from bs4 import BeautifulSoup, Comment

from logger import *

blacklisted_tags = [
    '[document]',
    'noscript',
    'header',
    'html',
    'meta',
    'head',
    'input',
    'script',
    'style',
    'title']


def _get_readable_for_human_content(sourcecode):
    debug("Parsing website data")

    output = ''
    soup = BeautifulSoup(sourcecode, 'html.parser')
    text = soup.find_all(text=True)

    for t in text:
        if t.parent.name not in blacklisted_tags:
            if type(t) is Comment:
                continue
            output += '{} '.format(t)

    return output


def _strip_punctuation(content):
    debug("Removing punctuation characters: " + string.punctuation)

    punctuation = string.punctuation + "„”–»"
    table = str.maketrans({key: None for key in punctuation})
    return content.translate(table)


def _replace_white_space_with_comma(content):
    debug("Creating comma separated words")

    return re.sub("\s+", ",", content).strip(",")


def retrieve_website_content(url):
    debug("Retrieving website data")

    response = requests.get(url)

    if response.status_code != 200:
        error("Response from the server has status code: %s" % response.status_code)
        error("Please try another URL")
        sys.exit()

    human_readable_content = _get_readable_for_human_content(response.content)

    stripped_punctuation_content = _strip_punctuation(human_readable_content)

    return _replace_white_space_with_comma(stripped_punctuation_content)
