from collections import Counter

from logger import *


def calculate_statistics(words, case_sensitive=False, splitter=','):
    debug("Calculating statistics")

    items = words.split(splitter) if case_sensitive else words.lower().split(splitter)

    return Counter(items)
