import logging

APP_NAME = "WordStats"

logger = logging.getLogger(APP_NAME)
formatter = logging.Formatter(APP_NAME + ': %(asctime)-15s %(message)s')

ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(formatter)

logger.addHandler(ch)
logger.setLevel(logging.DEBUG)


def debug(message):
    logger.debug(message)


def warning(message):
    logger.warning(message)


def error(message):
    logger.error(message)
